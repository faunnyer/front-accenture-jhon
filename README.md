

## Terminal Commands

1. Instalar node js [NodeJs Official Page](https://nodejs.org/en).
2. Abrir temrinal
3. ir al proyecto
4. correr en terminal ```npm install -g @angular/cli```
5. luego correr: ```npm install```
6. LEVANTAMOS EL BACK CON EL COMANDO  ```npm run start-back```
6. LEVANTAMOS EL FRONT CON EL COMANDO  ```npm run start```
7. navegamos en [http://localhost:4200/](http://localhost:4200/)



## Useful Links

More products from Creative Tim: <https://www.creative-tim.com/bootstrap-themes>

Tutorials: <https://www.youtube.com/channel/UCVyTG4sCw-rOvB9oHkzZD1w>

Freebies: <https://www.creative-tim.com/products>

Affiliate Program (earn money): <https://www.creative-tim.com/affiliates/new>

Social Media:

Twitter: <https://twitter.com/CreativeTim>

Facebook: <https://www.facebook.com/CreativeTim>

Dribbble: <https://dribbble.com/creativetim>

Google+: <https://plus.google.com/+CreativetimPage>

Instagram: <https://instagram.com/creativetimofficial>

[CHANGELOG]: ./CHANGELOG.md

[LICENSE]: ./LICENSE.md

[version-badge]: https://img.shields.io/badge/version-1.0.1-blue.svg

[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
