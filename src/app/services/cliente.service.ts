import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';


@Injectable()
export class ClienteService {

  //  urlClientes = 'http://localhost:8083/clientes';
  urlClientes = `${environment.api}/clientes`;

  constructor(private http: Http) { }

  getClientes() {
    return this.http.get(this.urlClientes).map(res => res.json());
  }

  getCliente(json: any): Observable<any> {
    return this.http.get(this.urlClientes + '/search/identificacion?id=' + json).map(res => res.json());
  }

  guardarCliente(json: any): Observable<any> {
    return this.http.post(this.urlClientes, json);
  }

}
