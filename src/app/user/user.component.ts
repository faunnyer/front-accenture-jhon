import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../../app/services/cliente.service';
import { Cliente } from '../model/cliente.class';
import swal from 'sweetalert';
import { Solicitud } from '../model/solicitud.class.';
import * as moment from 'moment';

@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit {

    clientes: any[] = [];


    cliente: Cliente = new Cliente();
    solicitud: Solicitud = new Solicitud();
    identificacionConsulta: number;


    constructor(private _ClientesService: ClienteService) { }

    ngOnInit() {
    }


    guardarCliente() {

        if (this.cliente.identificacion != null && this.cliente.nombre != null &&
            this.cliente.apellido != null && this.cliente.fechaNacimiento != null) {

                this._ClientesService.getCliente(this.cliente.identificacion).subscribe(
                    data => {
                        if (data._embedded.clientes.length === 0) {

                            let edad = moment().diff(this.cliente.fechaNacimiento, 'years');

                            if( edad > 18) {
                                this._ClientesService.guardarCliente(this.cliente).subscribe(
                                    data => {
                                        swal('Buen trabajo!', 'El cliente ha sido registrado exitosamente', 'success');
                                        this.limpiarcampos();
                                    }, err => {
                                        swal('Ha ocurrido un error!', 'error guardando cliente', 'error');
                                    });
                            }else {
                                swal('Un momento!', 'Para realizar una solicitud de credito debe de tener como minimo 18 años', 'warning');
                            }

                        }else {
                            swal('Este cliente ya se encuentra registrado!', 'Verifica tu numero de identificacion', 'error');
                        }
                    }, err => {
                        swal('Ha ocurrido un error!', 'error guardando cliente', 'error');
                    });
        } else {
            swal('Un momento!', 'Recuerda llenar todos los datos obligatorios', 'warning');
        }

    }


    validarSolicitud() {
        if (!(this.solicitud.empresa != null && this.solicitud.nit != null && this.solicitud.fechaIngreso != null &&
            this.solicitud.salario != null && this.solicitud.identificacionConsulta != null)) {
            swal('Un momento!', 'Recuerde Ingresar todos los campos', 'warning');
            return false;
        } else if (this.validacionFechaIngreso(this.solicitud.fechaIngreso)) {
            swal('Un momento ¡', 'Favor verificar la fecha de ingreso a la empresa', 'warning');
            return false;
        }else if (moment().diff(this.solicitud.fechaIngreso, 'month') < 6) {
            swal('Un momento ¡', 'Usted debe de tener como minimo 6 meses de antiguedad en la empresa', 'warning');
            return false;
        }

        this._ClientesService.getCliente(this.solicitud.identificacionConsulta).subscribe(
            data => {
                let notificacion = { title: 'Un momento', msg: 'Recuerda registrarte primero', status: 'warning' };
                if (this.solicitud.salario >= 10000000) {
                    notificacion.title = 'Lo sentimos';
                    notificacion.msg = 'El campo salario no debe superar los $100.000.000';
                    notificacion.status = 'warning';
                } else {
                    notificacion = data._embedded.clientes.length > 0 ? this.validacionCuposPorSalario(this.solicitud.salario) :
                        { title: 'Un momento', msg: 'Recuerda registrarte primero', status: 'warning' };
                }
                swal(notificacion.title, notificacion.msg, notificacion.status);
            }, err => {
                swal('Ha ocurrido un error!', 'error guardando cliente', 'error');
            });

    }

    validacionCuposPorSalario(salario: number) {
        let title = 'Estamos Listos!';
        let msg = 'Podrás acceder a un crédito por valor de: $';
        let status = 'success';

        if (salario >= 4000000) {
            msg = msg + '50.000.000';
        } else if (salario >= 1000000) {
            msg = msg + '20.000.000';
        } else if (salario >= 800000) {
            msg = msg + '5.000.000';
        } else {
            msg = 'No podras acceder a un credito';
            title = 'Lo sentimos';
            status = 'warning';
        }
        return { title, msg, status };
    }

    validacionFechaIngreso(fecha: any) {
        const fechaHoy = moment();
        const fechaIngresada = moment(fecha);
        return fechaHoy.isBefore(fechaIngresada);

    }


    limpiarcampos() {
        this.cliente.identificacion = null;
        this.cliente.apellido = '';
        this.cliente.fechaNacimiento = null;
        this.cliente.nombre = '';
    }
}
