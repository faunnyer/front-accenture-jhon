
export class Solicitud {
  cdsolicitud = 0;
  identificacionConsulta: number;
  empresa: string;
  nit: number;
  salario: number;
  fechaIngreso: Date;
}
